#!../../bin/linux-x86_64/new_ioc

< envPaths
epicsEnvSet("ENGINEER","John Doe")

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/new_ioc.dbd"
new_ioc_registerRecordDeviceDriver pdbbase

## Load record instances

#var streamDebug 1

cd "${TOP}/iocBoot/${IOC}"
iocInit

dbl > /opt/epics/ioc/log/logs.dbl
